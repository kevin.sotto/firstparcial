const pokemon = document.querySelector("#pokemon");

function fetchPokemon(id) {
  const _id = id + 232;
  fetch(`https://pokeapi.co/api/v2/pokemon/${_id}`)
    .then((res) => res.json())
    .then((data) => {
      createPokemon(data);
    });
}

function fetchPokemons(count) {
  for (let i = 1; i <= count; i++) {
    fetchPokemon(i);
  }
}

function createPokemon(poke) {
  const container = document.createElement("div");
  container.classList.add("image-container", "pokemon");

  const img = document.createElement("img");
  img.src = poke.sprites.front_default;
  container.append(img);

  pokemon.append(container);
}

fetchPokemons(12);
