const url =
  "https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=cfcc7c0d4c5bf02dec4997a0e482e26c&hash=76c0fcdf0d6b4f032f9345ec98a8722b";

fetch(url)
  .then((res) => res.json())
  .then((json) => {
    const marvel = document.querySelector("#marvel");
    for (const hero of json.data.results) {
      const container = document.createElement("div");
      container.classList.add("image-container", "marvel");

      const img = document.createElement("img");
      img.src = hero.thumbnail.path + ".jpg";
      container.append(img);

      marvel.append(container);
    }
  });
