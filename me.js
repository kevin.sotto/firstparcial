fetch("me.json")
  .then((response) => response.json())
  .then((data) => {
    document.getElementById(
      "name"
    ).innerHTML = `${data.firstName} ${data.lastName}`;
    document.getElementById("profesion").innerHTML = data.profesion;
    document.getElementById("studies-completed").innerHTML =
      data.studiesCompleted;
  });
